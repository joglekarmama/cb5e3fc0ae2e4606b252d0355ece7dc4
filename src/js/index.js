import Phaser from 'phaser';

import Game from './Game';
import PreGame from './PreGame';
// import PostGame from './PostGame';
// import LeaderBoard from './LeaderBoard';
// import PostLeaderBoard from './PostLeaderBoard';

let CONFIG;

CONFIG = {
  type: Phaser.AUTO,
  backgroundColor: '#000000',
  width: 740,
  height: 960,
  parent: 'phaser-example',
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
  },
  dom: {
    createContainer: true,
  },
  scene: [PreGame, Game],
  physics: {
    default: 'arcade',
    arcade: {
      // debug: true,
      gravity: { y: 100 },
    },
  },
};

let game = new Phaser.Game(CONFIG);
